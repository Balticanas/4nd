import numpy as np
import random
import matplotlib.pyplot as plt


a=3
b=-3
n = 1000000

x=np.random.uniform(a,b,n)

def f(x):
	return x**4

integral=0.0

for i in range(n):
	integral = integral+f(x[i])

ats=(a-b)/n*integral

err=ats-97.2

print("integral of x^4 from -3 to 3")
print("is equal to: 97.2")
print("Monte Carlo approximation:", ats)
print("the numeric error: ", err)
