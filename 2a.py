import numpy as np
import random
import matplotlib.pyplot as plt


a1=40**(1/4)
b1=-40**(1/4)

a2=(40/4)**(1/2)
b2=-(40/4)**(1/2)

a3=(40/6)**(1/2)
b3=-(40/6)**(1/2)

a4=(40/8)**(1/2)
b4=-(40/8)**(1/2)

print("The boundaries are: ")
print("from", "%.3f" %a1, "to", "%.3f" %b1, "for x1")
print("from", "%.3f" %a2, "to", "%.3f" %b2, "for x2")
print("from", "%.3f" %a3, "to", "%.3f" %b3, "for x3")
print("from", "%.3f" %a4, "to", "%.3f" %b4, "for x4")

