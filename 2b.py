import numpy as np
import random
import matplotlib.pyplot as plt

t=20 #how many times do we go

a1=40**(1/4)
b1=-40**(1/4)

a2=(40/4)**(1/2)
b2=-(40/4)**(1/2)

a3=(40/6)**(1/2)
b3=-(40/6)**(1/2)

a4=(40/8)**(1/2)
b4=-(40/8)**(1/2)

n = 50000 #dots that were fired. 50000 were enough to get the error < 1%
MC=[]
err=0
def f(x1,x2,x3,x4):
	return x1**4+4*x2**2+6*x3**2+8*x4**2


for j in range(0,t):
	
	x1=np.random.uniform(a1,b1,n)
	x2=np.random.uniform(a2,b2,n)
	x3=np.random.uniform(a3,b3,n)
	x4=np.random.uniform(a4,b4,n)
	
	m=0 #direct hits on the target before firing
	for i in range(n):
		if f(x1[i],x2[i],x3[i],x4[i])<40:
			m=m+1
	ats=(a1-b1)*(a2-b2)*(a3-b3)*(a4-b4)*m/n
	MC.append(ats)
	print("Monte Carlo approximation Nr.", j+1, " : ", ats)
	
	err=err+abs((MC[j]-MC[j-1])/(MC[j]+MC[j-1])*100) #skirtumas tarp dvieju integralu, padalintas is ju sumos vidurkio, kart procentai
	if abs((MC[j]-MC[j-1])/(MC[j]+MC[j-1])*100)>1:
		print("neuztenka suviu")

ERR=err/t
print("Absolute error average: ", "%.2f" %ERR, "%")
