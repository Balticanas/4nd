import numpy as np
import random
import matplotlib.pyplot as plt

Lx=-3.5 # x asies - riba. Kad butu visu tokia pati, lengviau lygint
Ly=-3.5 # y asies - riba
Rx= 3.5 # x asies + riba
Ry= 3.5 # y asies + riba

#12
S1=40**(1/4) # vertes, kurias gali igyti x1
x1=np.linspace(-S1, S1, num=500) # uzpildo x1 vertemis 
x2pos=(abs(40-x1**4)/4)**(0.5) # positive x2. abs, nes mete errorus, kad saknis neigiama :(
x2neg=-(abs(40-x1**4)/4)**(0.5) # negative x2
plt.subplot(231) # kad mestu i viena langa
plt.xlabel('$\mathregular{x_1}$') # x asies pavadinimas
plt.ylabel('$\mathregular{x_2}$') # y asies pavadinimas
plt.plot(x1, x2pos, 'b-') # atvaizduojama teigiama dalis
plt.plot(x1, x2neg, 'b-') # atvaizduojama neigiama dalis
plt.xlim( Lx, Rx) # visur tokie patys krastai
plt.ylim( Ly, Ry) # visur tokie patys krastai


#13
x1=np.linspace(-S1, S1, num=500)
x3pos=(abs(40-x1**4)/6)**(0.5)
x3neg=-(abs(40-x1**4)/6)**(0.5)
plt.subplot(232)
plt.xlabel('$\mathregular{x_1}$')
plt.ylabel('$\mathregular{x_3}$')
plt.plot(x1, x3pos, 'b-')
plt.plot(x1, x3neg, 'b-')
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#14
x1=np.linspace(-S1, S1, num=500)
x4pos=(abs(40-x1**4)/8)**(0.5)
x4neg=-(abs(40-x1**4)/8)**(0.5)
plt.subplot(233)
plt.xlabel('$\mathregular{x_1}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x1, x4pos, 'b-')
plt.plot(x1, x4neg, 'b-')
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#23
S2=(40/4)**0.5
x2=np.linspace(-S2, S2, num=500)
x3pos=(abs(40-4*x2**2)/6)**(0.5)
x3neg=-(abs(40-4*x2**2)/6)**(0.5)
plt.subplot(234)
plt.xlabel('$\mathregular{x_2}$')
plt.ylabel('$\mathregular{x_3}$')
plt.plot(x2, x3pos, 'r-')
plt.plot(x2, x3neg, 'r-')
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#24
x2=np.linspace(-S2, S2, num=500)
x4pos=(abs(40/8-4*x2**2/8))**(0.5)
x4neg=-(abs(40/8-4*x2**2/8))**(0.5)
plt.subplot(235)
plt.xlabel('$\mathregular{x_2}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x2, x4pos, 'r-')
plt.plot(x2, x4neg, 'r-')
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#34
S3=(40/6)**0.5
x3=np.linspace(-S3, S3, num=500)
x4pos=(abs(40-6*x3**2)/8)**(0.5)
x4neg=-(abs(40-6*x3**2)/8)**(0.5)
plt.subplot(236)
plt.xlabel('$\mathregular{x_3}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x3, x4pos, 'g-')
plt.plot(x3, x4neg, 'g-')
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

plt.tight_layout()
plt.savefig('2c.png')
plt.show()

