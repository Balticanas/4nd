import numpy as np
import random
import matplotlib.pyplot as plt

a1=40**(1/4)
b1=-40**(1/4)

a2=(40/4)**(1/2)
b2=-(40/4)**(1/2)

a3=(40/6)**(1/2)
b3=-(40/6)**(1/2)

a4=(40/8)**(1/2)
b4=-(40/8)**(1/2)
# x1, x2, x3, x4 ribos


def f(x1,x2,x3,x4):
	return x1**4+4*x2**2+6*x3**2+8*x4**2

Xm1=[]
Xm2=[]
Xm3=[]
Xm4=[]

R12=[]
R13=[]
R14=[]
R23=[]
R24=[]
R34=[]

R12sort=[]
R13sort=[]
R14sort=[]
R23sort=[]
R24sort=[]
R34sort=[]

x3taskas12=[]
x4taskas12=[]
x2taskas13=[]
x4taskas13=[]
x2taskas14=[]
x3taskas14=[]
x1taskas23=[]
x4taskas23=[]
x1taskas24=[]
x3taskas24=[]
x1taskas34=[]
x2taskas34=[]


m=0 # confirmed hits on the target before the shootout
n=0 # shots fired before the shootout. reikia 2552348 suviu, kad butu 1000000 pataikymu

while(m<10**6): # gonna spin till there are a million confirmed hits. **6!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	x1=np.random.uniform(a1,b1)
	x2=np.random.uniform(a2,b2)
	x3=np.random.uniform(a3,b3)
	x4=np.random.uniform(a4,b4)

	n=n+1

	if f(x1,x2,x3,x4)<40:
		Xm1.append(x1)
		Xm2.append(x2)
		Xm3.append(x3)
		Xm4.append(x4)
		m=m+1
		R12.append((x1**2+x2**2)**0.5)
		R13.append((x1**2+x3**2)**0.5)
		R14.append((x1**2+x4**2)**0.5)
		R23.append((x2**2+x3**2)**0.5)
		R24.append((x2**2+x4**2)**0.5)
		R34.append((x3**2+x4**2)**0.5)
print("done 1")
M=10**5 # 10% of 10**6
	
R12sort=sorted(R12)
R13sort=sorted(R13)
R14sort=sorted(R14)
R23sort=sorted(R23)
R24sort=sorted(R24)
R34sort=sorted(R34)

c12=R12sort[M] # R12 verte, kuri savyje apima 10% visu m tasku
c13=R13sort[M]
c14=R14sort[M]
c23=R23sort[M]
c24=R24sort[M]
c34=R34sort[M]

print("done 2")
for i in range(0,m):
	if R12[i]<c12:
		x3taskas12.append(Xm3[i])
		x4taskas12.append(Xm4[i])
	if R13[i]<c13:
		x2taskas13.append(Xm2[i])	
		x4taskas13.append(Xm4[i])
	if R14[i]<c14:
		x2taskas14.append(Xm2[i])
		x3taskas14.append(Xm3[i])
	if R23[i]<c23:
		x1taskas23.append(Xm1[i])
		x4taskas23.append(Xm4[i])
	if R24[i]<c24:
		x1taskas24.append(Xm1[i])
		x3taskas24.append(Xm3[i])
	if R34[i]<c34:
		x1taskas34.append(Xm1[i])
		x2taskas34.append(Xm2[i])
print("n", n)
print("done 3")

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2c kodas, tik prie plotu prideta taskai

Lx=-3.5 # x asies - riba. Kad butu visu tokia pati, lengviau lygint
Ly=-3.5 # y asies - riba
Rx= 3.5 # x asies + riba
Ry= 3.5 # y asies + riba

#12
S1=40**(1/4) # vertes, kurias gali igyti x1
x1=np.linspace(-S1, S1, num=500) # uzpildo x1 vertemis 
x2pos=(abs(40-x1**4)/4)**(0.5) # positive x2. abs, nes mete errorus, kad saknis neigiama :(
x2neg=-(abs(40-x1**4)/4)**(0.5) # negative x2
plt.subplot(231) # kad mestu i viena langa
plt.xlabel('$\mathregular{x_1}$') # x asies pavadinimas
plt.ylabel('$\mathregular{x_2}$') # y asies pavadinimas
plt.plot(x1, x2pos, 'b-') # atvaizduojama teigiama dalis
plt.plot(x1, x2neg, 'b-') # atvaizduojama neigiama dalis
plt.scatter(x1taskas34,x2taskas34, s=0.0001)
plt.xlim( Lx, Rx) # visur tokie patys krastai
plt.ylim( Ly, Ry) # visur tokie patys krastai


#13
x1=np.linspace(-S1, S1, num=500)
x3pos=(abs(40-x1**4)/6)**(0.5)
x3neg=-(abs(40-x1**4)/6)**(0.5)
plt.subplot(232)
plt.xlabel('$\mathregular{x_1}$')
plt.ylabel('$\mathregular{x_3}$')
plt.plot(x1, x3pos, 'b-')
plt.plot(x1, x3neg, 'b-')
plt.scatter(x1taskas24,x3taskas24, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#14
x1=np.linspace(-S1, S1, num=500)
x4pos=(abs(40-x1**4)/8)**(0.5)
x4neg=-(abs(40-x1**4)/8)**(0.5)
plt.subplot(233)
plt.xlabel('$\mathregular{x_1}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x1, x4pos, 'b-')
plt.plot(x1, x4neg, 'b-')
plt.scatter(x1taskas23,x4taskas23, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#23
S2=(40/4)**0.5
x2=np.linspace(-S2, S2, num=500)
x3pos=(abs(40-4*x2**2)/6)**(0.5)
x3neg=-(abs(40-4*x2**2)/6)**(0.5)
plt.subplot(234)
plt.xlabel('$\mathregular{x_2}$')
plt.ylabel('$\mathregular{x_3}$')
plt.plot(x2, x3pos, 'r-')
plt.plot(x2, x3neg, 'r-')
plt.scatter(x2taskas14,x3taskas14, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#24
x2=np.linspace(-S2, S2, num=500)
x4pos=(abs(40/8-4*x2**2/8))**(0.5)
x4neg=-(abs(40/8-4*x2**2/8))**(0.5)
plt.subplot(235)
plt.xlabel('$\mathregular{x_2}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x2, x4pos, 'r-')
plt.plot(x2, x4neg, 'r-')
plt.scatter(x2taskas13,x4taskas13, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#34
S3=(40/6)**0.5
x3=np.linspace(-S3, S3, num=500)
x4pos=(abs(40-6*x3**2)/8)**(0.5)
x4neg=-(abs(40-6*x3**2)/8)**(0.5)
plt.subplot(236)
plt.xlabel('$\mathregular{x_3}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x3, x4pos, 'g-')
plt.plot(x3, x4neg, 'g-')
plt.scatter(x3taskas12,x4taskas12, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

plt.tight_layout()
plt.savefig('2ci.png')
plt.show()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
print("done 4")

