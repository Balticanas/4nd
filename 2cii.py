import numpy as np
import random
import matplotlib.pyplot as plt

a1=40**(1/4)
b1=-40**(1/4)

a2=(40/4)**(1/2)
b2=-(40/4)**(1/2)

a3=(40/6)**(1/2)
b3=-(40/6)**(1/2)

a4=(40/8)**(1/2)
b4=-(40/8)**(1/2)


def f(x1,x2,x3,x4):
	return x1**4+4*x2**2+6*x3**2+8*x4**2

X1=[]
X2=[]
X3=[]
X4=[]

X1m12=[]
X2m12=[]
X3m12=[]
X4m12=[]

X1m13=[]
X2m13=[]
X3m13=[]
X4m13=[]

X1m14=[]
X2m14=[]
X3m14=[]
X4m14=[]

X1m23=[]
X2m23=[]
X3m23=[]
X4m23=[]

X1m24=[]
X2m24=[]
X3m24=[]
X4m24=[]

X1m34=[]
X2m34=[]
X3m34=[]
X4m34=[]



m1=0 #direct hits on the target before firing
n1=0
while(m1<10**5): # gonna spin till there are a 100,000 confirmed hits.
	x1=np.random.uniform(a1,b1)
	x2=np.random.uniform(a2,b2)
	n1=n1+1
	if f(x1,x2,0,0)<40:
		X1m12.append(x1)
		X2m12.append(x2)
		m1=m1+1
m2=0
n2=0
while(m2<10**5):
	x1=np.random.uniform(a1,b1)
	x3=np.random.uniform(a3,b3)
	n2=n2+1
	if f(x1,0,x3,0)<40:
		X1m13.append(x1)
		X3m13.append(x3)
		m2=m2+1
m3=0
n3=0
while(m3<10**5):
	x1=np.random.uniform(a1,b1)
	x4=np.random.uniform(a4,b4)
	n3=n3+1
	if f(x1,0,0,x4)<40:
		X1m14.append(x1)
		X4m14.append(x4)
		m3=m3+1
n4=0
m4=0
while(m4<10**5):
	x2=np.random.uniform(a2,b2)
	x3=np.random.uniform(a3,b3)
	n4=n4+1
	if f(0,x2,x3,0)<40:
		X2m23.append(x2)
		X3m23.append(x3)
		m4=m4+1
n5=0
m5=0
while(m5<10**5):
	x2=np.random.uniform(a2,b2)
	x4=np.random.uniform(a4,b4)
	n5=n5+1
	if f(0,x2,0,x4)<40:
		X2m24.append(x2)
		X4m24.append(x4)
		m5=m5+1
n6=0
m6=0
while(m6<10**5):
	x3=np.random.uniform(a3,b3)
	x4=np.random.uniform(a4,b4)
	n6=n6+1
	if f(0,0,x3,x4)<40:
		X3m34.append(x3)
		X4m34.append(x4)
		m6=m6+1





#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# 2c kodas, tik prie plotu prideta taskai
Lx=-3.5 # x asies - riba. Kad butu visu tokia pati, lengviau lygint
Ly=-3.5 # y asies - riba
Rx= 3.5 # x asies + riba
Ry= 3.5 # y asies + riba

#12
S1=40**(1/4) # vertes, kurias gali igyti x1
x1=np.linspace(-S1, S1, num=500) # uzpildo x1 vertemis 
x2pos=(abs(40-x1**4)/4)**(0.5) # positive x2. abs, nes mete errorus, kad saknis neigiama :(
x2neg=-(abs(40-x1**4)/4)**(0.5) # negative x2
plt.subplot(231) # kad mestu i viena langa
plt.xlabel('$\mathregular{x_1}$') # x asies pavadinimas
plt.ylabel('$\mathregular{x_2}$') # y asies pavadinimas
plt.plot(x1, x2pos, 'b-') # atvaizduojama teigiama dalis
plt.plot(x1, x2neg, 'b-') # atvaizduojama neigiama dalis
plt.scatter(X1m12, X2m12, s=0.0001)
plt.xlim( Lx, Rx) # visur tokie patys krastai
plt.ylim( Ly, Ry) # visur tokie patys krastai


#13
x1=np.linspace(-S1, S1, num=500)
x3pos=(abs(40-x1**4)/6)**(0.5)
x3neg=-(abs(40-x1**4)/6)**(0.5)
plt.subplot(232)
plt.xlabel('$\mathregular{x_1}$')
plt.ylabel('$\mathregular{x_3}$')
plt.plot(x1, x3pos, 'b-')
plt.plot(x1, x3neg, 'b-')
plt.scatter(X1m13, X3m13, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#14
x1=np.linspace(-S1, S1, num=500)
x4pos=(abs(40-x1**4)/8)**(0.5)
x4neg=-(abs(40-x1**4)/8)**(0.5)
plt.subplot(233)
plt.xlabel('$\mathregular{x_1}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x1, x4pos, 'b-')
plt.plot(x1, x4neg, 'b-')
plt.scatter(X1m14, X4m14, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#23
S2=(40/4)**0.5
x2=np.linspace(-S2, S2, num=500)
x3pos=(abs(40-4*x2**2)/6)**(0.5)
x3neg=-(abs(40-4*x2**2)/6)**(0.5)
plt.subplot(234)
plt.xlabel('$\mathregular{x_2}$')
plt.ylabel('$\mathregular{x_3}$')
plt.plot(x2, x3pos, 'r-')
plt.plot(x2, x3neg, 'r-')
plt.scatter(X2m23, X3m23, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#24
x2=np.linspace(-S2, S2, num=500)
x4pos=(abs(40/8-4*x2**2/8))**(0.5)
x4neg=-(abs(40/8-4*x2**2/8))**(0.5)
plt.subplot(235)
plt.xlabel('$\mathregular{x_2}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x2, x4pos, 'r-')
plt.plot(x2, x4neg, 'r-')
plt.scatter(X2m24, X4m24, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

#34
S3=(40/6)**0.5
x3=np.linspace(-S3, S3, num=500)
x4pos=(abs(40-6*x3**2)/8)**(0.5)
x4neg=-(abs(40-6*x3**2)/8)**(0.5)
plt.subplot(236)
plt.xlabel('$\mathregular{x_3}$')
plt.ylabel('$\mathregular{x_4}$')
plt.plot(x3, x4pos, 'g-')
plt.plot(x3, x4neg, 'g-')
plt.scatter(X3m34, X4m34, s=0.0001)
plt.xlim( Lx, Rx)
plt.ylim( Ly, Ry)

plt.tight_layout()
plt.savefig('2cii.png')
plt.show()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

