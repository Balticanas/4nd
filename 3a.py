import pandas as pd
import random
import matplotlib.pyplot as plt
import numpy as np

binai=250

def data():
    df_events = pd.read_csv('pp_4l_all.csv', decimal=',', skipinitialspace=True)
    df_events['E1'] = pd.to_numeric(df_events['E1'], errors='coerce')
    df_events['p1x'] = pd.to_numeric(df_events['p1x'], errors='coerce')
    df_events['p1y'] = pd.to_numeric(df_events['p1y'], errors='coerce')
    df_events['p1z'] = pd.to_numeric(df_events['p1z'], errors='coerce')
    df_events['E2'] = pd.to_numeric(df_events['E2'], errors='coerce')
    df_events['p2x'] = pd.to_numeric(df_events['p2x'], errors='coerce')
    df_events['p2y'] = pd.to_numeric(df_events['p2y'], errors='coerce')
    df_events['p2z'] = pd.to_numeric(df_events['p2z'], errors='coerce')
    df_events['E3'] = pd.to_numeric(df_events['E3'], errors='coerce')
    df_events['p3x'] = pd.to_numeric(df_events['p3x'], errors='coerce')
    df_events['p3y'] = pd.to_numeric(df_events['p3y'], errors='coerce')
    df_events['p3z'] = pd.to_numeric(df_events['p3z'], errors='coerce')
    df_events['E4'] = pd.to_numeric(df_events['E4'], errors='coerce')
    df_events['p4x'] = pd.to_numeric(df_events['p4x'], errors='coerce')
    df_events['p4y'] = pd.to_numeric(df_events['p4y'], errors='coerce')
    df_events['p4z'] = pd.to_numeric(df_events['p4z'], errors='coerce')
    return(df_events)

data=data()


M=[]
for i in range (0,len(data)):
	E1=data['E1'][i]
	E2=data['E2'][i]
	E3=data['E3'][i]
	E4=data['E4'][i]

	px1=data['p1x'][i]
	px2=data['p2x'][i]
	px3=data['p3x'][i]
	px4=data['p4x'][i]

	py1=data['p1y'][i]
	py2=data['p2y'][i]
	py3=data['p3y'][i]
	py4=data['p4y'][i]

	pz1=data['p1z'][i]
	pz2=data['p2z'][i]
	pz3=data['p3z'][i]
	pz4=data['p4z'][i]

	Ei=E1+E2+E3+E4
	pxi=px1+px2+px3+px4
	pyi=py1+py2+py3+py4
	pzi=pz1+pz2+pz3+pz4

	Mi=(Ei**2-(pxi**2+pyi**2+pzi**2))**0.5

	M.append(Mi)


plt.hist(M, bins=binai)
plt.xlabel('m') 
plt.ylabel('Count')
plt.savefig('3a.png')
plt.show()

