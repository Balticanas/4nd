import pandas as pd
import random
import matplotlib.pyplot as plt
import numpy as np

binai=50

def data():
    df_events = pd.read_csv('pp_4l_all.csv', decimal=',', skipinitialspace=True)
    df_events['E1'] = pd.to_numeric(df_events['E1'], errors='coerce')
    df_events['p1x'] = pd.to_numeric(df_events['p1x'], errors='coerce')
    df_events['p1y'] = pd.to_numeric(df_events['p1y'], errors='coerce')
    df_events['p1z'] = pd.to_numeric(df_events['p1z'], errors='coerce')
    df_events['E2'] = pd.to_numeric(df_events['E2'], errors='coerce')
    df_events['p2x'] = pd.to_numeric(df_events['p2x'], errors='coerce')
    df_events['p2y'] = pd.to_numeric(df_events['p2y'], errors='coerce')
    df_events['p2z'] = pd.to_numeric(df_events['p2z'], errors='coerce')
    df_events['E3'] = pd.to_numeric(df_events['E3'], errors='coerce')
    df_events['p3x'] = pd.to_numeric(df_events['p3x'], errors='coerce')
    df_events['p3y'] = pd.to_numeric(df_events['p3y'], errors='coerce')
    df_events['p3z'] = pd.to_numeric(df_events['p3z'], errors='coerce')
    df_events['E4'] = pd.to_numeric(df_events['E4'], errors='coerce')
    df_events['p4x'] = pd.to_numeric(df_events['p4x'], errors='coerce')
    df_events['p4y'] = pd.to_numeric(df_events['p4y'], errors='coerce')
    df_events['p4z'] = pd.to_numeric(df_events['p4z'], errors='coerce')
    return(df_events)

data=data()


PT=[]
Eta1=[]
Eta2=[]
Eta3=[]
Eta4=[]

for i in range (0,len(data)):
	E1=data['E1'][i]
	E2=data['E2'][i]
	E3=data['E3'][i]
	E4=data['E4'][i]

	px1=data['p1x'][i]
	px2=data['p2x'][i]
	px3=data['p3x'][i]
	px4=data['p4x'][i]

	py1=data['p1y'][i]
	py2=data['p2y'][i]
	py3=data['p3y'][i]
	py4=data['p4y'][i]

	pz1=data['p1z'][i]
	pz2=data['p2z'][i]
	pz3=data['p3z'][i]
	pz4=data['p4z'][i]

	Ei=E1+E2+E3+E4
	pxi=px1+px2+px3+px4
	pyi=py1+py2+py3+py4
	pzi=pz1+pz2+pz3+pz4

	Mi=(Ei**2-(pxi**2+pyi**2+pzi**2))**0.5

	
	if Mi>100 and Mi<150:

		pt1=(px1**2+py1**2)**0.5
		pt2=(px2**2+py2**2)**0.5
		pt3=(px3**2+py3**2)**0.5
		pt4=(px4**2+py4**2)**0.5

		PT=[pt1,pt2,pt3,pt4]
		PT.sort(reverse=True)
		
		Pt1=PT[0] # didzioji P rodo, kad Pt1 yra susortintas pt1
		Pt2=PT[1]
		Pt3=PT[2]
		Pt4=PT[3]

		p1=(px1**2+py1**2+pz1**2)**0.5
		p2=(px2**2+py2**2+pz2**2)**0.5
		p3=(px3**2+py3**2+pz3**2)**0.5
		p4=(px4**2+py4**2+pz4**2)**0.5

		P=[p1,p2,p3,p4]
		P.sort(reverse=True)
		
		P1=P[0]
		P2=P[1]
		P3=P[2]
		P4=P[3]


		eta1=1/2*np.log((P1+Pt1)/(P1-Pt1))
		eta2=1/2*np.log((P2+Pt2)/(P2-Pt2))
		eta3=1/2*np.log((P3+Pt3)/(P3-Pt3))
		eta4=1/2*np.log((P4+Pt4)/(P4-Pt4))
		
		Eta1.append(eta1)
		Eta2.append(eta2)
		Eta3.append(eta3)
		Eta4.append(eta4)

Lx=100
Rx=150


plt.subplot(221) # kad mestu i viena langa
plt.xlabel('$\mathregular{eta_1}$') # x asies pavadinimas
plt.ylabel('$\mathregular{Count of events}$') # y asies pavadinimas
plt.hist(Eta1, bins=binai, fc=(0, 0, 1 ,1))

plt.subplot(222) 
plt.xlabel('$\mathregular{eta_2}$') 
plt.ylabel('$\mathregular{Count of events}$')
plt.hist(Eta2, bins=binai, fc=(0, 1, 0 ,1))

plt.subplot(223) 
plt.xlabel('$\mathregular{eta_3}$') 
plt.ylabel('$\mathregular{Count of events}$')
plt.hist(Eta3, bins=binai, fc=(1, 0, 0 ,1))

plt.subplot(224) 
plt.xlabel('$\mathregular{eta_4}$') 
plt.ylabel('$\mathregular{Count of events}$')
plt.hist(Eta4, bins=binai, fc=(0, 0, 0 ,1)) # fc= (Red, Green, Blue, Transparency)

plt.tight_layout()
plt.savefig('3biii.png')
plt.show()
