import pandas as pd
import random
import matplotlib.pyplot as plt
import numpy as np

binai=100

def data():
    df_events = pd.read_csv('pp_4l_all.csv', decimal=',', skipinitialspace=True)
    df_events['E1'] = pd.to_numeric(df_events['E1'], errors='coerce')
    df_events['p1x'] = pd.to_numeric(df_events['p1x'], errors='coerce')
    df_events['p1y'] = pd.to_numeric(df_events['p1y'], errors='coerce')
    df_events['p1z'] = pd.to_numeric(df_events['p1z'], errors='coerce')
    df_events['E2'] = pd.to_numeric(df_events['E2'], errors='coerce')
    df_events['p2x'] = pd.to_numeric(df_events['p2x'], errors='coerce')
    df_events['p2y'] = pd.to_numeric(df_events['p2y'], errors='coerce')
    df_events['p2z'] = pd.to_numeric(df_events['p2z'], errors='coerce')
    df_events['E3'] = pd.to_numeric(df_events['E3'], errors='coerce')
    df_events['p3x'] = pd.to_numeric(df_events['p3x'], errors='coerce')
    df_events['p3y'] = pd.to_numeric(df_events['p3y'], errors='coerce')
    df_events['p3z'] = pd.to_numeric(df_events['p3z'], errors='coerce')
    df_events['E4'] = pd.to_numeric(df_events['E4'], errors='coerce')
    df_events['p4x'] = pd.to_numeric(df_events['p4x'], errors='coerce')
    df_events['p4y'] = pd.to_numeric(df_events['p4y'], errors='coerce')
    df_events['p4z'] = pd.to_numeric(df_events['p4z'], errors='coerce')
    return(df_events)

data=data()

All=[]
signal=[]
noise=[]
M=[]
for i in range (0,len(data)):
	E1=data['E1'][i]
	E2=data['E2'][i]
	E3=data['E3'][i]
	E4=data['E4'][i]

	px1=data['p1x'][i]
	px2=data['p2x'][i]
	px3=data['p3x'][i]
	px4=data['p4x'][i]

	py1=data['p1y'][i]
	py2=data['p2y'][i]
	py3=data['p3y'][i]
	py4=data['p4y'][i]

	pz1=data['p1z'][i]
	pz2=data['p2z'][i]
	pz3=data['p3z'][i]
	pz4=data['p4z'][i]

	Ei=E1+E2+E3+E4
	pxi=px1+px2+px3+px4
	pyi=py1+py2+py3+py4
	pzi=pz1+pz2+pz3+pz4
	
	Mi=(Ei**2-(pxi**2+pyi**2+pzi**2))**0.5


	if Mi>100 and Mi<150:
		All.append(Mi)
	if Mi>124 and Mi<126:
		signal.append(Mi)
	if Mi>100 and Mi<124 or Mi>126 and Mi<150 :
		noise.append(Mi)
nvid=len(noise)/((124-100)+(150-126)) # noise vidurkis

integrated_rates_ruozas=(len(signal)-nvid*(126-124))/len(All)*100
integrated_rates_visas_signalas=(len(signal)-nvid*(126-124))/len(data)*100
SNR=(len(signal)-nvid*(126-124))/len(noise)*100
print("integrated rates of the signal and the noise from 100 to 150:")
print( "%.3f" %integrated_rates_ruozas, "%")
print("integrated rates of the signal and the noise from all the data")
print( "%.3f" %integrated_rates_visas_signalas, "%")
print("Signal to Noise Ratio:")
print("%.3f" %SNR, "%")
plt.hist(All, bins=binai)
plt.xlabel('m') 
plt.ylabel('Count')
plt.savefig('3c.png')
plt.show()
